//
//  ConverterRatesDataSourceTests.swift
//  RevoluttTestAppTests
//
//  Created by Константин on 22/02/2019.
//  Copyright © 2019 YK. All rights reserved.
//

import XCTest
@testable import RevoluttTestApp

class ConverterRatesDataSourceTests: XCTestCase {
    
    var dataSource: ConverterRatesDataSource = CachedConverterRatesDataSource()
    fileprivate let dataSourceOutput = TestDataSourceOutput()
    
    var fetchingExp: XCTestExpectation?

    override func setUp() {
        dataSource.output = dataSourceOutput
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testIfCachedDataSourceHasBaseCurrencyEURAndFirstRateAUDwithRatio16199() {
        dataSourceOutput.fetchingExp = expectation(description: "fetching")
        
        dataSource.startFetching()
        wait(for: [dataSourceOutput.fetchingExp!], timeout: 0.1)
        XCTAssert(dataSourceOutput.ratesInfo?.baseCurrency == "EUR",
                  "baseCurrency: \(String(describing:  dataSourceOutput.ratesInfo?.baseCurrency))")
        let firstRate = dataSourceOutput.ratesInfo!.rates[0]
        XCTAssert(firstRate.targetCurrency == "AUD",
                  "firstRate.targetCurrency: \(firstRate.targetCurrency)")
        XCTAssert(firstRate.ratio == 1.6199,
                  "firstRate.ratio: \(firstRate.ratio)")
    }
}

fileprivate class TestDataSourceOutput: ConverterRatesDataSourceOutput {
    var fetchingExp: XCTestExpectation?
    
    func didReceive(ratesInfo: ConverterRatesInfo) {
        self.ratesInfo = ratesInfo
        fetchingExp?.fulfill()
    }
    
    func didReceive(error: Error) {
        
    }
    
    var ratesInfo: ConverterRatesInfo?
}
