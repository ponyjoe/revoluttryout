//
//  MoneyInputViewModelTests.swift
//  RevoluttTestAppTests
//
//  Created by Константин on 20/02/2019.
//  Copyright © 2019 YK. All rights reserved.
//

import XCTest
@testable import RevoluttTestApp

class MoneyInputViewModelOutputMock: MoneyInputViewModelOutput {
    func didEndEdit(vm: MoneyInputViewModel) {
        // none
    }
    
    func didEdit(vm: MoneyInputViewModel) {
        // none
    }
    
    func didBeginEdit(vm: MoneyInputViewModel) {
        // none
    }
}

class MoneyInputViewModelTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testIfTextualInputFails() {
        var vm = MoneyInputViewModel(currency: "USD",
                                     amount: 100,
                                     state: .display,
                                     output: MoneyInputViewModelOutputMock())
        
        let editting: MoneyInputViewModel.Edit = .init()
        vm.state = .edit(editting)
        
        do {
            vm = try editting.edit(vm: vm, inputText: "10csf1")
        } catch let error as MoneyInputViewModel.Edit.Error {
            
            switch error {
            case .notNumber:
                break
            default:
                XCTAssertFalse(false)
            }
        } catch _ {
            XCTAssertFalse(false)
        }
        
        XCTAssertFalse(false)
    }
    
    func testIfInputOfNum128AcceptedAndEquals128() {
        var vm = MoneyInputViewModel(currency: "USD",
                                     amount: 100,
                                     state: .display,
                                     output: MoneyInputViewModelOutputMock())
        
        let editting: MoneyInputViewModel.Edit = .init()
        vm.state = .edit(editting)
        
        do {
            vm = try editting.edit(vm: vm, inputText: "128")
        } catch _ as MoneyInputViewModel.Edit.Error {
            
            XCTAssertFalse(false)
        } catch _ {
            XCTAssertFalse(false)
        }
        
        XCTAssert(vm.amount == 128, "amount: \(vm.amount)")
    }
    
    func testIfTwoViewModelsAreEqual() {
        let vm1 = MoneyInputViewModel(currency: "USD",
                                     amount: 15,
                                     state: .display,
                                     output: MoneyInputViewModelOutputMock())
        let vm2 = MoneyInputViewModel(currency: "USD",
                                     amount: 15,
                                     state: .display,
                                     output: MoneyInputViewModelOutputMock())
        
        XCTAssert(vm1 == vm2)
    }
    
    func testIfTwoViewModelsAreNOTEqual() {
        var vm1 = MoneyInputViewModel(currency: "USD",
                                      amount: 15,
                                      state: .display,
                                      output: MoneyInputViewModelOutputMock())
        var vm2 = MoneyInputViewModel(currency: "USD",
                                      amount: 14,
                                      state: .display,
                                      output: MoneyInputViewModelOutputMock())
        
        XCTAssert(vm1 != vm2)
        
        vm1 = MoneyInputViewModel(currency: "USD",
                                      amount: 15,
                                      state: .display,
                                      output: MoneyInputViewModelOutputMock())
        vm2 = MoneyInputViewModel(currency: "RUB",
                                      amount: 15,
                                      state: .display,
                                      output: MoneyInputViewModelOutputMock())
        
        XCTAssert(vm1 != vm2)
        
        vm1 = MoneyInputViewModel(currency: "USD",
                                  amount: 15,
                                  state: .display,
                                  output: MoneyInputViewModelOutputMock())
        vm2 = MoneyInputViewModel(currency: "USD",
                                  amount: 15,
                                  state: .edit(.init()),
                                  output: MoneyInputViewModelOutputMock())
        
        XCTAssert(vm1 != vm2)
    }
}
