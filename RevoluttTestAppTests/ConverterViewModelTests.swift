//
//  ConverterViewModelTests.swift
//  RevoluttTestAppTests
//
//  Created by Константин on 22/02/2019.
//  Copyright © 2019 YK. All rights reserved.
//

import XCTest
@testable import RevoluttTestApp

class ConverterViewModelTests: XCTestCase {
    
    var vm: ConverterViewModel = .init(
        moneyInputs: [],
        rates: [],
        output: MockConverterViewModelOutput()
    )

    override func setUp() {
        vm.rates = [
            ConverterRate(
                baseCurrency: "EUR",
                targetCurrency: "RUB",
                ratio: 79.47
            ),
            ConverterRate(
                baseCurrency: "EUR",
                targetCurrency: "JPY",
                ratio: 129.38
            ),
        ]
    }

    override func tearDown() {
    }

    func testIfNecesseryMoneyInputsExists() {
        XCTAssert(vm.moneyInputs.count == vm.rates.count, "vm.moneyInputs.count: \(vm.moneyInputs.count)")
    }
    
    func testIf1000RUBUpdatesJPYto1628() {
        let currency = "RUB"
        vm.targetCurrency = currency
        vm.setMoneyInput(withCurrency: currency, toAmount: 1000)
        vm.update()
        
        XCTAssert(Int(vm.moneyInputs[1].amount) == 1628, "JPY: \(Int(vm.moneyInputs[1].amount))")
    }
    
    func testIfJPYChangesHaveNoEffectWhileTargetIsRUB() {
        vm.targetCurrency = "RUB"
        vm.setMoneyInput(withCurrency: "JPY", toAmount: 282)
        vm.update()
        
        XCTAssert(vm.moneyInputs[0].amount == 79.47, "RUB: \(vm.moneyInputs[0].amount)")
        XCTAssert(vm.moneyInputs[1].amount == 129.38, "JPY: \(vm.moneyInputs[1].amount)")
    }
}

class MockConverterViewModelOutput: ConverterViewModelOutput {
    func didDeselect(currency: CurrencyCode) {}
    
    func didChange(currency: CurrencyCode, input: Float) {}
    
    func didSelect(currency: CurrencyCode) {}
}
