//
//  ConverterViewController.swift
//  RevoluttTestApp
//
//  Created by Константин on 14/02/2019.
//  Copyright © 2019 YK. All rights reserved.
//

import UIKit

class ConverterViewController: UIViewController {
    
    var router = ConverterRouter.init()
    
    override func loadView() {
        view = router.buildConverter()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        /*let view = router.buildConverter()
        view.translatesAutoresizingMaskIntoConstraints = true
        self.view.addSubview(view)
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]*/
    }
}

