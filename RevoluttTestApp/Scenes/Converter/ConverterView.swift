//
//  ConverterView.swift
//  RevoluttTestApp
//
//  Created by Константин on 21/02/2019.
//  Copyright © 2019 YK. All rights reserved.
//

import UIKit

class ConverterView: UIView {
    var vm: ConverterViewModel {
        didSet(oldVM) {
            DispatchQueue.main.async { [weak self] in
                guard let strongSelf = self else { return }
                guard !strongSelf.tableView.isDragging && !strongSelf.tableView.isDecelerating else {
                    return
                }
                guard !strongSelf.isTableAnimating else { return }
                
                strongSelf.update(by: (strongSelf.vm, oldVM))
            }
        }
    }
    
    init(viewModel vm: ConverterViewModel) {
        self.vm = vm
        
        super.init(frame: .zero)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private let tableView: UITableView = UITableView()
    private var isTableAnimating: Bool = false
}

extension ConverterView {
    func update(by vm: (
                    new: ConverterViewModel,
                    old: ConverterViewModel)) {
        
        if vm.new.moneyInputs.count != vm.old.moneyInputs.count {
            tableView.backgroundView?.isHidden = !vm.new.moneyInputs.isEmpty
            tableView.reloadData()
        } else {
            if
                vm.new.targetCurrency != vm.old.targetCurrency,
                let newTargetCurrency = vm.new.targetCurrency,
                newTargetCurrency != vm.old.targetCurrency,
                let _ = vm.old.index(ofCurrency: newTargetCurrency),
                let _ = vm.new.index(ofCurrency: newTargetCurrency) {
                
                updateRowsExceptEditable(by: vm.new)
                
            } else {
                updateRowsExceptEditable(by: vm.new)
            }
        }
    }
    
    func updateRowsExceptEditable(by vm: ConverterViewModel) {
        let indexPathsToReload: [IndexPath] = vm.moneyInputs
            .enumerated()
            .filter { !$0.element.state.isEdit }
            .map { IndexPath(row: $0.offset, section: 0) }
            .map { $0 }
        
        
        UIView.performWithoutAnimation {
            tableView.reloadRows(at: indexPathsToReload, with: .none)
        }
    }
}

extension ConverterView {
    func setup() {
        setupLayout()
        setupTableView()
        setupKeyboardHandling()
        
        backgroundColor = .white
    }
    
    func setupLayout() {
        addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        
        [
            tableView.leftAnchor.constraint(equalTo: safeAreaLayoutGuide.leftAnchor, constant: 0),
            tableView.rightAnchor.constraint(equalTo: safeAreaLayoutGuide.rightAnchor, constant: 0),
            tableView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor)
        ].forEach {
            $0.isActive = true
        }
    }
    
    func setupTableView() {
        tableView.dataSource = self
        tableView.register(MoneyInputCell.self, forCellReuseIdentifier: MoneyInputCell.reuseIdentifier)
        tableView.rowHeight = 104
        tableView.estimatedRowHeight = 104
        tableView.separatorStyle = .none
        tableView.keyboardDismissMode = .onDrag
        tableView.delegate = self
        tableView.backgroundColor = .white
        tableView.backgroundView = ConverterNoDataView()
    }
    
    func setupKeyboardHandling() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notif:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notif:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
}

extension ConverterView {
    @objc func keyboardWillShow(notif: Notification) {
        guard let keyboardSize = notif.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? CGRect else {
            return
        }
        
        let contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
        
        guard let appearDuration = notif.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? TimeInterval else {
            return
        }
        
        
        
        isTableAnimating = true
        
        UIView.animate(withDuration: appearDuration, delay: 0, options: .curveEaseIn, animations: { [weak self, weak tableView] in
            tableView?.contentInset = contentInsets
            tableView?.scrollIndicatorInsets = contentInsets
            
            if let targetCurrency = self?.vm.targetCurrency,
                let newFocusIndex = self?.vm.index(ofCurrency: targetCurrency) {
                tableView?.scrollToRow(at: IndexPath(row: newFocusIndex, section: 0), at: .top, animated: false)
            }
        }, completion: { [weak self] finished in
            guard finished else { return }
            
            self?.isTableAnimating = false
        })
    }
    
    @objc func keyboardWillHide(notif: Notification) {
        guard let appearDuration = notif.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? TimeInterval else {
            return
        }
        UIView.animate(withDuration: appearDuration, delay: 0, options: .curveEaseIn, animations: { [weak tableView] in
            tableView?.contentInset = .zero
            tableView?.scrollIndicatorInsets = .zero
        }, completion: nil)
    }
}

extension ConverterView: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vm.moneyInputs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let _cell = tableView.dequeueReusableCell(withIdentifier: MoneyInputCell.reuseIdentifier, for: indexPath) as? MoneyInputCell
        
        guard let cell = _cell else {
            fatalError("ConverterView: wrong cell type")
        }
        
        cell.vm = vm.moneyInputs[indexPath.row]
        
        return cell
    }
}

extension ConverterView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
