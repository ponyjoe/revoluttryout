//
//  ConverterNoDataView.swift
//  RevoluttTestApp
//
//  Created by Константин on 22/02/2019.
//  Copyright © 2019 YK. All rights reserved.
//

import UIKit

class ConverterNoDataView: UIView {
    
    private let activityIndicator: UIActivityIndicatorView = .init(style: .gray)
    private let title: UILabel = .init()

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        
        activityIndicator.startAnimating()
        
        title.text = "No data yet..."
        title.textColor = .gray
        title.font = UIFont.systemFont(ofSize: 28, weight: .light)
        
        setupLayout()
    }
    
    fileprivate func setupLayout() {
        let stack = UIStackView(arrangedSubviews: [title, activityIndicator])
        stack.axis = .vertical
        stack.spacing = 16
        stack.alignment = .center
        
        addSubview(stack)
        stack.translatesAutoresizingMaskIntoConstraints = false
        
        stack.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        stack.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        stack.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
    }
}
