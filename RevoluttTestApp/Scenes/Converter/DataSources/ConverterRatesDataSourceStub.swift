//
//  ConverterRatesDataSourceStub.swift
//  RevoluttTestApp
//
//  Created by Константин on 22/02/2019.
//  Copyright © 2019 YK. All rights reserved.
//

import Foundation

struct ConverterRatesDataSourceStub: ConverterRatesDataSource {
    var mapper: ConverterRatesInfoMapping
    
    var rates: [ConverterRate] {
        return [
            ConverterRate(
                baseCurrency: "EUR",
                targetCurrency: "RUB",
                ratio: 79.47
            ),
            ConverterRate(
                baseCurrency: "EUR",
                targetCurrency: "JPY",
                ratio: 129.38
            ),
            ConverterRate(
                baseCurrency: "EUR",
                targetCurrency: "GBP",
                ratio: 0.89705
            ),
        ]
    }
    
    var output: ConverterRatesDataSourceOutput
    
    func fetch(completion: @escaping ([String : Any]) -> ()) {
        // nothing
    }
}

struct ConverterRatesDataSourceOutputStub: ConverterRatesDataSourceOutput {
    func didReceive(error: Error) {
        // nothing
    }
    
    func didReceive(ratesInfo: ConverterRatesInfo) {
        // nothing
    }
}
