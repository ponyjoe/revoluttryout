//
//  ConverterRateMapperV1.swift
//  RevoluttTestApp
//
//  Created by Константин on 22/02/2019.
//  Copyright © 2019 YK. All rights reserved.
//

import Foundation

extension ConverterRate {
    struct MapperV1: ConverterRateMapping {
        func map(json: [String : Any], baseCurrency: CurrencyCode) throws -> ConverterRate {
            guard
                let currency = json.keys.first,
                let ratio = json[currency] as? Double
                else {
                    throw Error.invalidFormat
            }
            
            //let ratioTruncatedTo4Places = (Float(ratio)*10000).rounded(.toNearestOrEven)/10000
            
            return ConverterRate(
                baseCurrency: baseCurrency,
                targetCurrency: currency,
                ratio: Float(ratio)
            )
        }
    }
}

extension ConverterRate.MapperV1 {
    enum Error: Swift.Error {
        case invalidFormat
        case unknown
    }
}

extension ConverterRate.MapperV1.Error {
    var localizedDescription: String {
        switch self {
        case .invalidFormat:
            return "Invalid json format"
        default:
            return "Unknown error"
        }
    }
}
