//
//  ConverterRatesInfoMapperV1.swift
//  RevoluttTestApp
//
//  Created by Константин on 22/02/2019.
//  Copyright © 2019 YK. All rights reserved.
//

import Foundation

extension ConverterRatesInfo {
    struct MapperV1: ConverterRatesInfoMapping {
        var rateMapper: ConverterRateMapping
        
        func map(json: [String : Any]) throws -> ConverterRatesInfo {
            guard
                let baseCurrency = json["base"] as? CurrencyCode,
                let date = json["date"] as? String,
                let rawRates = json["rates"] as? [String: Any]
            else {
                throw Error.invalidFormat
            }
            
            let rawRatesArray = rawRates.reversed().reversed()
            
            return ConverterRatesInfo(
                baseCurrency: baseCurrency,
                date: date,
                rates: try rawRatesArray
                    .map {
                        try rateMapper.map(
                            json: [$0.0: $0.1],
                            baseCurrency: baseCurrency
                        )
                    }
                    .sorted(by: { $0.targetCurrency < $1.targetCurrency })
            )
        }
    }
}

extension ConverterRatesInfo.MapperV1 {
    enum Error: Swift.Error {
        case invalidFormat
        case unknown
    }
}

extension ConverterRatesInfo.MapperV1.Error {
    var localizedDescription: String {
        switch self {
        case .invalidFormat:
            return "Invalid json format"
        default:
            return "Unknown error"
        }
    }
}
