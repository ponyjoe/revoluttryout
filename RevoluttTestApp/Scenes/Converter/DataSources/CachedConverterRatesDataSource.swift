//
//  CachedConverterRatesDataSource.swift
//  RevoluttTestApp
//
//  Created by Константин on 22/02/2019.
//  Copyright © 2019 YK. All rights reserved.
//

import Foundation

struct CachedConverterRatesDataSource: ConverterRatesDataSource {
    var mapper: ConverterRatesInfoMapping
    var output: ConverterRatesDataSourceOutput
    
    func fetch(completion: @escaping ([String : Any]) -> ()) {
        let jsonString = """
        {"base":"EUR","date":"2018-09-06","rates":{"AUD":1.6199,"BGN":1.9601,"BRL":4.8022,"CAD":1.5371,"CHF":1.13,"CNY":7.9624,"CZK":25.771,"DKK":7.4729,"GBP":0.90019,"HKD":9.1523,"HRK":7.4503,"HUF":327.2,"IDR":17361.0,"ILS":4.1797,"INR":83.9,"ISK":128.08,"JPY":129.83,"KRW":1307.6,"MXN":22.414,"MYR":4.8225,"NOK":9.7973,"NZD":1.7671,"PHP":62.728,"PLN":4.3277,"RON":4.6486,"RUB":79.748,"SEK":10.614,"SGD":1.6035,"THB":38.213,"TRY":7.6448,"USD":1.1659,"ZAR":17.862}}
        """
        
        let json = try! JSONSerialization.jsonObject(with: jsonString.data(using: .utf8)!, options: JSONSerialization.ReadingOptions.allowFragments)
        
        guard let jsonDict = json as? [String: Any] else {
            return
        }
        
        completion(jsonDict)
    }
    
    init() {
        output = ConverterRatesDataSourceOutputStub()
        mapper = ConverterRatesInfo.MapperV1(
            rateMapper: ConverterRate.MapperV1()
        )
    }
    
    
}
