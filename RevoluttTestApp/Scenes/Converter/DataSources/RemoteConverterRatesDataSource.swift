//
//  RemoteConverterRatesDataSource.swift
//  RevoluttTestApp
//
//  Created by Константин on 22/02/2019.
//  Copyright © 2019 YK. All rights reserved.
//

import Foundation

fileprivate struct Constants {
    static let defaultURL: URL = URL(string: "https://revolut.duckdns.org/latest?base=EUR")!
}

struct RemoteConverterRatesDataSource: ConverterRatesDataSource {
    var mapper: ConverterRatesInfoMapping
    var output: ConverterRatesDataSourceOutput
    var cache: ConverterRatesDataSource
    var url: URL
    var session: URLSession { return URLSession.shared }
    
    init(mapper: ConverterRatesInfoMapping = mapperV1,
         cache: ConverterRatesDataSource = CachedConverterRatesDataSource(),
         output: ConverterRatesDataSourceOutput,
         url: URL = Constants.defaultURL) {
        
        self.mapper = mapper
        self.cache = cache
        self.output = output
        self.url = url
        
        //self.cache.output = self
    }
    
    func fetch(completion: @escaping ([String : Any]) -> ()) {
        let task = session.dataTask(with: url) { (data, response, error) in
            do {
                guard let data = data else {
                    assertionFailure("no data")
                    return
                }
                guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] else {
                    assertionFailure("not a valid json")
                    return
                }
                
                completion(json)
            } catch let error as NSError {
                print(error.debugDescription)
            } catch _ {
                print("unkown error")
            }
        }
        
        task.resume()
    }
}

fileprivate var mapperV1: ConverterRatesInfoMapping {
    return ConverterRatesInfo.MapperV1(
        rateMapper: ConverterRate.MapperV1()
    )
}
