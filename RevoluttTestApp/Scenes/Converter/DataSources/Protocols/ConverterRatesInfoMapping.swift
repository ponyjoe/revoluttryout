//
//  ConverterRatesInfoMapping.swift
//  RevoluttTestApp
//
//  Created by Константин on 22/02/2019.
//  Copyright © 2019 YK. All rights reserved.
//

import Foundation

protocol ConverterRatesInfoMapping {
    var rateMapper: ConverterRateMapping { get set }
    func map(json: [String: Any]) throws -> ConverterRatesInfo
}
