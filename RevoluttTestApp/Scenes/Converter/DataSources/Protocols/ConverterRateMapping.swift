//
//  ConverterRateMapping.swift
//  RevoluttTestApp
//
//  Created by Константин on 22/02/2019.
//  Copyright © 2019 YK. All rights reserved.
//

import Foundation

protocol ConverterRateMapping {
    func map(json: [String: Any], baseCurrency: CurrencyCode) throws -> ConverterRate
}
