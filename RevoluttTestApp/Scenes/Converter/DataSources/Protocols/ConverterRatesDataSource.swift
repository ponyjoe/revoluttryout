//
//  ConverterRatesDataSource.swift
//  RevoluttTestApp
//
//  Created by Константин on 22/02/2019.
//  Copyright © 2019 YK. All rights reserved.
//

import Foundation

protocol ConverterRatesDataSource {
    var mapper: ConverterRatesInfoMapping { get set }
    var output: ConverterRatesDataSourceOutput { get set }
    /// Do not use directly, prefer `startFetching()`
    func fetch(completion: @escaping ([String: Any]) -> ())
}

extension ConverterRatesDataSource {
    func startFetching() {
        fetch(completion: map(rawData:))
    }
    
    func map(rawData: [String: Any]) {
        do {
            let ratesInfo = try mapper.map(json: rawData)
            output.didReceive(ratesInfo: ratesInfo)
        } catch let error {
            output.didReceive(error: error)
        }
    }
}

protocol ConverterRatesDataSourceOutput {
    func didReceive(ratesInfo: ConverterRatesInfo)
    func didReceive(error: Error)
}
