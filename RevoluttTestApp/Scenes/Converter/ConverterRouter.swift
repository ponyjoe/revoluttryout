//
//  ConverterRouter.swift
//  RevoluttTestApp
//
//  Created by Константин on 14/02/2019.
//  Copyright © 2019 YK. All rights reserved.
//

import Foundation
import UIKit

/// TODO: Implement
class ConverterRouter {
    weak var currentVC: UIViewController?
    weak var view: ConverterView?
    var dataSource: ConverterRatesDataSource?
    weak var timer: Timer?
    
    func buildConverter() -> UIView {
        let dataSource = RemoteConverterRatesDataSource(output: self)
        
        let vm = ConverterViewModel(
            moneyInputs: [],
            rates: [],
            output: self
        )
        
        let view = ConverterView(viewModel: vm)
        self.view = view
        
        self.dataSource = dataSource
        
        DispatchQueue.global(qos: .background).async { [weak self] in
            let timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { (timer) in
                self?.dataSource?.startFetching()
            }
            let runLoop = RunLoop.current
            runLoop.add(timer, forMode: .default)
            runLoop.run()
        }
        
        return view
    }
}

extension ConverterRouter: ConverterRouting {
    func recalculate(basedOn balance: (CurrencyCode, Float)) {
        view?.vm.update(byFocusedMoneyInput: balance)
    }
    
    func recalculate(withRates rates: [ConverterRate]) {
        view?.vm.rates = rates
    }
    
    func recalculate() {
        view?.vm.update()
    }
    
    func bringCurrencyToTop(_ currency: CurrencyCode) {
        view?.vm.targetCurrency = currency
    }
    
    func show(error: Error) {
        let alert = UIAlertController(title: "Error accured", message: "\(error.localizedDescription)", preferredStyle: .alert)
        currentVC?.present(alert, animated: true, completion: nil)
    }
}

extension ConverterRouter: ConverterViewModelOutput {
    func didDeselect(currency: CurrencyCode) {
        view?.vm.targetCurrency = nil
    }
}

extension ConverterRouter: ConverterRatesDataSourceOutput {}


