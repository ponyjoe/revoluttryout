//
//  ConverterRatesDataSourceOutput+Routing.swift
//  RevoluttTestApp
//
//  Created by Константин on 22/02/2019.
//  Copyright © 2019 YK. All rights reserved.
//

import Foundation

extension ConverterRatesDataSourceOutput where Self: ConverterRouting {
    func didReceive(ratesInfo: ConverterRatesInfo) {
        recalculate(withRates: ratesInfo.rates)
    }
    
    func didReceive(error: Error) {
        show(error: error)
    }
}
