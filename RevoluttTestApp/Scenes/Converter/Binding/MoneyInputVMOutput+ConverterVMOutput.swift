//
//  MoneyInputVMOutput+ConverterVMOutput.swift
//  RevoluttTestApp
//
//  Created by Константин on 21/02/2019.
//  Copyright © 2019 YK. All rights reserved.
//

import Foundation

extension MoneyInputViewModelOutput where Self: ConverterViewModelOutput {
    func didEdit(vm: MoneyInputViewModel) {
        didChange(currency: vm.currency, input: vm.amount)
    }
    
    func didBeginEdit(vm: MoneyInputViewModel) {
        didSelect(currency: vm.currency)
    }
    
    func didEndEdit(vm: MoneyInputViewModel) {
        didDeselect(currency: vm.currency)
    }
}
