//
//  ConverterViewModelOutput+Routing.swift
//  RevoluttTestApp
//
//  Created by Константин on 15/02/2019.
//  Copyright © 2019 YK. All rights reserved.
//

import Foundation

extension ConverterViewModelOutput where Self: ConverterRouting {
    func didChange(currency: CurrencyCode, input: Float) {
        recalculate(basedOn: (currency, input))
    }
    
    func didSelect(currency: CurrencyCode) {
        bringCurrencyToTop(currency)
    }
}
