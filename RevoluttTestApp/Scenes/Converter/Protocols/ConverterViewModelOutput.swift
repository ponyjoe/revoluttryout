//
//  ConverterViewModelOutput.swift
//  RevoluttTestApp
//
//  Created by Константин on 15/02/2019.
//  Copyright © 2019 YK. All rights reserved.
//

import Foundation

protocol ConverterViewModelOutput: MoneyInputViewModelOutput {
    func didChange(currency: CurrencyCode, input: Float)
    func didSelect(currency: CurrencyCode)
    func didDeselect(currency: CurrencyCode)
}
