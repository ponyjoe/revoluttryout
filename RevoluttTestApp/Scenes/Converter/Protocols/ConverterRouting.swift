//
//  ConverterRouting.swift
//  RevoluttTestApp
//
//  Created by Константин on 15/02/2019.
//  Copyright © 2019 YK. All rights reserved.
//

import Foundation

protocol ConverterRouting {
    func recalculate(basedOn balance: (CurrencyCode, Float))
    func recalculate(withRates rates: [ConverterRate])
    func recalculate()
    func bringCurrencyToTop(_ currency: CurrencyCode)
    func show(error: Error)
}
