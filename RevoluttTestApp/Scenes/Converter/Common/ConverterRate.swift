//
//  ConverterRate.swift
//  RevoluttTestApp
//
//  Created by Константин on 21/02/2019.
//  Copyright © 2019 YK. All rights reserved.
//

import Foundation

struct ConverterRate {
    var baseCurrency: String = "EUR"
    var targetCurrency: String = "USD"
    var ratio: Float = 1.0
}
