//
//  ConverterRatesInfo.swift
//  RevoluttTestApp
//
//  Created by Константин on 22/02/2019.
//  Copyright © 2019 YK. All rights reserved.
//

import Foundation

struct ConverterRatesInfo {
    let baseCurrency: CurrencyCode
    let date: String
    let rates: [ConverterRate]
}
