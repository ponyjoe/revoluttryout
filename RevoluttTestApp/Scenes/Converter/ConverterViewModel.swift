//
//  ConverterViewModel.swift
//  RevoluttTestApp
//
//  Created by Константин on 14/02/2019.
//  Copyright © 2019 YK. All rights reserved.
//

import Foundation

typealias CurrencyCode = String

struct ConverterViewModel {
    private(set)
    var moneyInputs: [MoneyInputViewModel]
    var baseCurrency: CurrencyCode {
        return rates.first?.baseCurrency ?? "EUR"
    }
    var rates: [ConverterRate] {
        didSet {
            update()
        }
    }
    weak var output: ConverterViewModelOutput?
}

// MARK: - Focused Money Input
extension ConverterViewModel {    
    var focusedMoneyInput: MoneyInputViewModel? {
        get {
            return moneyInputs.first(where: { $0.state.isEdit })
        }
        
        set {
            guard var focus = newValue else {
                if let oldTargetCurrency = targetCurrency,
                    let oldFocusIndex = index(ofCurrency: oldTargetCurrency) {
                    var newMoneyInputs = moneyInputs
                    let output = newMoneyInputs[oldFocusIndex].output
                    newMoneyInputs[oldFocusIndex].output = nil
                    newMoneyInputs[oldFocusIndex].state = .display
                    newMoneyInputs[oldFocusIndex].output = output
                    
                    moneyInputs = newMoneyInputs
                }
                
                return
            }
            
            if let oldTargetCurrency = targetCurrency,
                let oldFocusIndex = index(ofCurrency: oldTargetCurrency) {
                
                moneyInputs[oldFocusIndex].state = .display
            }
            
            let output = focus.output
            focus.output = nil
            focus.startEdit()
            focus.output = output
            
            guard let focusIndex = index(ofCurrency: focus.currency) else {
                return
            }
            
            moneyInputs[focusIndex] = focus
        }
    }
    
    var targetCurrency: CurrencyCode? {
        get {
            return focusedMoneyInput?.currency
        }
        
        set(newTargetCurrency) {
            let focusedMoneyInput =
                moneyInputs
                    .first(where: {
                        $0.currency == newTargetCurrency
                    })
            
            self.focusedMoneyInput = focusedMoneyInput
        }
    }
    
    func index(ofCurrency currency: CurrencyCode) -> Int? {
        guard let index = moneyInputs.firstIndex(where: { $0.currency == currency }) else {
            return nil
        }
        
        return index
    }
}

// MARK: - Update block
extension ConverterViewModel {
    mutating func update() {
        if focusedMoneyInput != nil {
            updateByCurrentInput()
        } else {
            updateByAnyInput()
        }
    }
    
    mutating func updateByAnyInput() {
        guard let targetMoneyInput = moneyInputs.last else {
            update(byBalanceInBaseCurrency: 1)
            return
        }
        
        update(byFocusedMoneyInput: (targetMoneyInput.currency, targetMoneyInput.amount))
    }
    
    mutating func updateByCurrentInput() {
        guard let focusedInput = focusedMoneyInput else {
            return
        }
        
        update(byFocusedMoneyInput: (focusedInput.currency, focusedInput.amount))
    }
    
    mutating func update(byFocusedMoneyInput input: (currency: CurrencyCode, amount: Float)) {
        guard let focusedRate = rates.first(where: { $0.targetCurrency == input.currency }) else {
            assertionFailure("no mathing rate currency \(input.currency)")
            return
        }
        
        let amountInBaseCurrency = input.amount / focusedRate.ratio
        update(byBalanceInBaseCurrency: amountInBaseCurrency)
    }
    
    mutating func update(byBalanceInBaseCurrency balance: Float) {
        for rate in rates {
            let newAmount = balance * rate.ratio
            
            setMoneyInput(withCurrency: rate.targetCurrency, toAmount: newAmount)
        }
    }
}

extension ConverterViewModel {
    mutating func setMoneyInput(withCurrency currency: CurrencyCode, toAmount amount: Float) {
        guard let index = index(ofCurrency: currency) else {
            createMoneyInput(withCurrency: currency, amount: amount)
            
            return
        }
        
        let output = moneyInputs[index].output
        moneyInputs[index].output = nil
        moneyInputs[index].amount = amount
        moneyInputs[index].output = output
    }
    
    mutating func createMoneyInput(withCurrency currency: CurrencyCode, amount: Float) {
        
        let newInput = MoneyInputViewModel(
            currency: currency,
            amount: amount,
            state: .display,
            output: self.output
        )
        moneyInputs.append(newInput)
    }
}

