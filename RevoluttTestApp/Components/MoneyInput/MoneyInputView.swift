//
//  MoneyInputView.swift
//  RevoluttTestApp
//
//  Created by Константин on 21/02/2019.
//  Copyright © 2019 YK. All rights reserved.
//

import UIKit
import Kingfisher

fileprivate struct Constants {
    static let currencyIconWidth: CGFloat = 38
}

class MoneyInputView: UIView {
    var vm: MoneyInputViewModel {
        didSet {
            guard vm != oldValue else {
                return
            }
            update(by: vm)
        }
    }
    
    init(viewModel vm: MoneyInputViewModel) {
        self.vm = vm
        
        super.init(frame: .zero)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private var currencyIcon: UIImageView = .init(image: nil)
    private var currencyTitle: UILabel = .init()
    private var currencySubtitle: UILabel = .init()
    private var amountInput: UITextField = .init()
    private var amountInputGuide: UIView = .init()
}

extension MoneyInputView {
    func update(by vm: MoneyInputViewModel) {
        let countryCode: String = country2Currency
            .filter { $0.value == vm.currency }
            .sorted { $0.key < $1.key }
            .first!.key
        
        let currencyDescription = Locale.current.localizedString(forCurrencyCode: vm.currency)
        
        let currencyImgUrl = URL(
            string: "https://www.countryflags.io/\(countryCode)/shiny/64.png")!
        
        currencyIcon.kf.setImage(with: currencyImgUrl)
        currencyIcon.kf.indicatorType = .activity
        currencyIcon.layer.cornerRadius = Constants.currencyIconWidth/2
        currencyIcon.layer.masksToBounds = true
        
        currencyTitle.text = vm.currency
        currencySubtitle.text = currencyDescription
        amountInput.text = vm.amount != 0
            ? String(describing: vm.amount)
            : String(describing: 0)
        
        
        amountInputGuide.backgroundColor = vm.state.isEdit ? tintColor : .gray
        
        if !vm.state.isEdit && amountInput.isFirstResponder {
            amountInput.resignFirstResponder()
        } else if vm.state.isEdit && !amountInput.isFirstResponder {
            amountInput.becomeFirstResponder()
        } else {
            // nothing
        }
    }
}

extension MoneyInputView {
    func setup() {
        setupLayout()
        
        amountInput.keyboardType = .decimalPad
        amountInput.delegate = self
        
        currencyTitle.font = UIFont.systemFont(ofSize: 14)
        currencySubtitle.font = UIFont.systemFont(ofSize: 12)
        currencySubtitle.textColor = .gray
        
        
    }
    
    func setupLayout() {
        let currencyInfo = setupCurrencyInfoLayout()
        let amountInfo: UIView = {
            let stack = UIStackView(
                arrangedSubviews: [UIView(), amountInput, UIView()]
            )
            stack.axis = .vertical
            stack.alignment = .trailing
            stack.distribution = .equalCentering
            
            return stack
        }()
        
        amountInputGuide.translatesAutoresizingMaskIntoConstraints = false
        amountInputGuide.heightAnchor.constraint(equalToConstant: 1).isActive = true
        amountInput.addSubview(amountInputGuide)
        amountInputGuide.bottomAnchor.constraint(equalTo: amountInput.bottomAnchor).isActive = true
        amountInputGuide.leftAnchor.constraint(equalTo: amountInput.leftAnchor).isActive = true
        amountInputGuide.rightAnchor.constraint(equalTo: amountInput.rightAnchor).isActive = true
        
        
        let rootStack: UIStackView = {
            let stack = UIStackView(
                arrangedSubviews: [currencyInfo, UIView(), amountInfo]
            )
            stack.axis = .horizontal
            stack.distribution = .fill
            stack.alignment = .fill
            
            return stack
        }()
        
        addSubview(rootStack)
        rootStack.translatesAutoresizingMaskIntoConstraints = false
        [
            rootStack.leftAnchor.constraint(equalTo: leftAnchor),
            rootStack.rightAnchor.constraint(equalTo: rightAnchor),
            rootStack.topAnchor.constraint(equalTo: topAnchor),
            rootStack.bottomAnchor.constraint(equalTo: bottomAnchor)
        ].forEach {
                $0.isActive = true
        }
    }
    
    func setupCurrencyInfoLayout() -> UIView {
        let rightColumn: UIStackView = {
            let stack = UIStackView(
                arrangedSubviews: [currencyTitle,
                                   //UIView(),
                                   currencySubtitle]
            )
            stack.axis = .vertical
            stack.spacing = 6
            //stack.distribution = .fill
            stack.alignment = .leading
            
            return stack
        }()
        
        let leftColumn = currencyIcon
        leftColumn.widthAnchor.constraint(equalToConstant: Constants.currencyIconWidth).isActive = true
        leftColumn.heightAnchor.constraint(equalTo: leftColumn.widthAnchor).isActive = true
        currencyIcon.contentMode = .center
        
        let rootStack: UIStackView = {
            let stack = UIStackView(
                arrangedSubviews: [leftColumn, rightColumn]
            )
            stack.axis = .horizontal
            stack.alignment = .center
            stack.spacing = 14
            stack.distribution = .fill
            
            return stack
        }()
        
        return rootStack
    }
}

extension MoneyInputView: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        vm.startEdit()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        vm.state = .display
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let currText = textField.text else {
            return true
        }
        
        let oldText = NSString(string: currText)
        let newText = oldText.replacingCharacters(in: range, with: string)
        
        guard case .edit(let editting) = vm.state else {
            fatalError("textField is active whilst vm is not in editting state!")
        }
        
        do {
            let newVM = try editting.edit(vm: self.vm, inputText: newText)
            
            DispatchQueue.main.async {
                self.vm = newVM
            }
            
            return true
        } catch let error as MoneyInputViewModel.Edit.Error {
            switch error {
            case .lastCharacterIsPoint:
                return true
            default:
                // TODO: ...
                return false
            }
            // TODO: show errors
        } catch _ {
            // unknown errors
            return false
        }
    }
}

class MoneyInputCell: UITableViewCell {
    static var reuseIdentifier: String = "MoneyInputCellID"
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        
        let vm: MoneyInputViewModel = self.vm ?? self.defaultVM
        let view = MoneyInputView(viewModel: vm)
        
        contentView.addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        
        [
            view.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 8),
            view.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -8),
            view.topAnchor.constraint(equalTo: contentView.topAnchor),
            view.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
            ].forEach {
                $0.isActive = true
        }
        
        self.view = view
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var view: MoneyInputView?
    
    var defaultVM: MoneyInputViewModel {
        return MoneyInputViewModel(currency: "RUB", amount: 79, state: .display, output: MoneyInputViewModelOutputStub())
    }
    
    var vm: MoneyInputViewModel? {
        get {
            return view?.vm
        }
        
        set {
            guard let newVM = newValue else {
                return
            }
            guard let view = view else {
                fatalError("MoneyInputCell: no view found!")
            }
            
            view.vm = newVM
        }
    }
}


fileprivate let country2Currency: [String: String] = ["BD": "BDT", "BE": "EUR", "BF": "XOF", "BG": "BGN", "BA": "BAM", "BB": "BBD", "WF": "XPF", "BL": "EUR", "BM": "BMD", "BN": "BND", "BO": "BOB", "BH": "BHD", "BI": "BIF", "BJ": "XOF", "BT": "BTN", "JM": "JMD", "BV": "NOK", "BW": "BWP", "WS": "WST", "BQ": "USD", "BR": "BRL", "BS": "BSD", "JE": "GBP", "BY": "BYR", "BZ": "BZD", "RU": "RUB", "RW": "RWF", "RS": "RSD", "TL": "USD", "RE": "EUR", "TM": "TMT", "TJ": "TJS", "RO": "RON", "TK": "NZD", "GW": "XOF", "GU": "USD", "GT": "GTQ", "GS": "GBP", "GR": "EUR", "GQ": "XAF", "GP": "EUR", "JP": "JPY", "GY": "GYD", "GG": "GBP", "GF": "EUR", "GE": "GEL", "GD": "XCD", "GB": "GBP", "GA": "XAF", "SV": "USD", "GN": "GNF", "GM": "GMD", "GL": "DKK", "GI": "GIP", "GH": "GHS", "OM": "OMR", "TN": "TND", "JO": "JOD", "HR": "HRK", "HT": "HTG", "HU": "HUF", "HK": "HKD", "HN": "HNL", "HM": "AUD", "VE": "VEF", "PR": "USD", "PS": "ILS", "PW": "USD", "PT": "EUR", "SJ": "NOK", "PY": "PYG", "IQ": "IQD", "PA": "PAB", "PF": "XPF", "PG": "PGK", "PE": "PEN", "PK": "PKR", "PH": "PHP", "PN": "NZD", "PL": "PLN", "PM": "EUR", "ZM": "ZMK", "EH": "MAD", "EE": "EUR", "EG": "EGP", "ZA": "ZAR", "EC": "USD", "IT": "EUR", "VN": "VND", "SB": "SBD", "ET": "ETB", "SO": "SOS", "ZW": "ZWL", "SA": "SAR", "ES": "EUR", "ER": "ERN", "ME": "EUR", "MD": "MDL", "MG": "MGA", "MF": "EUR", "MA": "MAD", "MC": "EUR", "UZ": "UZS", "MM": "MMK", "ML": "XOF", "MO": "MOP", "MN": "MNT", "MH": "USD", "MK": "MKD", "MU": "MUR", "MT": "EUR", "MW": "MWK", "MV": "MVR", "MQ": "EUR", "MP": "USD", "MS": "XCD", "MR": "MRO", "IM": "GBP", "UG": "UGX", "TZ": "TZS", "MY": "MYR", "MX": "MXN", "IL": "ILS", "FR": "EUR", "IO": "USD", "SH": "SHP", "FI": "EUR", "FJ": "FJD", "FK": "FKP", "FM": "USD", "FO": "DKK", "NI": "NIO", "NL": "EUR", "NO": "NOK", "NA": "NAD", "VU": "VUV", "NC": "XPF", "NE": "XOF", "NF": "AUD", "NG": "NGN", "NZ": "NZD", "NP": "NPR", "NR": "AUD", "NU": "NZD", "CK": "NZD", "XK": "EUR", "CI": "XOF", "CH": "CHF", "CO": "COP", "CN": "CNY", "CM": "XAF", "CL": "CLP", "CC": "AUD", "CA": "CAD", "CG": "XAF", "CF": "XAF", "CD": "CDF", "CZ": "CZK", "CY": "EUR", "CX": "AUD", "CR": "CRC", "CW": "ANG", "CV": "CVE", "CU": "CUP", "SZ": "SZL", "SY": "SYP", "SX": "ANG", "KG": "KGS", "KE": "KES", "SS": "SSP", "SR": "SRD", "KI": "AUD", "KH": "KHR", "KN": "XCD", "KM": "KMF", "ST": "STD", "SK": "EUR", "KR": "KRW", "SI": "EUR", "KP": "KPW", "KW": "KWD", "SN": "XOF", "SM": "EUR", "SL": "SLL", "SC": "SCR", "KZ": "KZT", "KY": "KYD", "SG": "SGD", "SE": "SEK", "SD": "SDG", "DO": "DOP", "DM": "XCD", "DJ": "DJF", "DK": "DKK", "VG": "USD", "DE": "EUR", "YE": "YER", "DZ": "DZD", "US": "USD", "UY": "UYU", "YT": "EUR", "UM": "USD", "LB": "LBP", "LC": "XCD", "LA": "LAK", "TV": "AUD", "TW": "TWD", "TT": "TTD", "TR": "TRY", "LK": "LKR", "LI": "CHF", "LV": "EUR", "TO": "TOP", "LT": "LTL", "LU": "EUR", "LR": "LRD", "LS": "LSL", "TH": "THB", "TF": "EUR", "TG": "XOF", "TD": "XAF", "TC": "USD", "LY": "LYD", "VA": "EUR", "VC": "XCD", "AE": "AED", "AD": "EUR", "AG": "XCD", "AF": "AFN", "AI": "XCD", "VI": "USD", "IS": "ISK", "IR": "IRR", "AM": "AMD", "AL": "ALL", "AO": "AOA", "AQ": "", "AS": "USD", "AR": "ARS", "AU": "AUD", "AT": "EUR", "AW": "AWG", "IN": "INR", "AX": "EUR", "AZ": "AZN", "IE": "EUR", "ID": "IDR", "UA": "UAH", "QA": "QAR", "MZ": "MZN"]
