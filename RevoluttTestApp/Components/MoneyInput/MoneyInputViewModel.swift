//
//  MoneyInputViewModel.swift
//  RevoluttTestApp
//
//  Created by Константин on 15/02/2019.
//  Copyright © 2019 YK. All rights reserved.
//

import Foundation


struct MoneyInputViewModel {
    let currency: CurrencyCode
    
    var amount: Float {
        didSet {
            guard amount != oldValue else {
                return
            }
            guard case .edit = state else {
                return
            }
            
            output?.didEdit(vm: self)
        }
    }
    
    var state: State {
        didSet {
            if case .display = oldValue, case .edit = state {
                output?.didBeginEdit(vm: self)
            }
            
            if case .edit = oldValue, case .display = state {
                output?.didEndEdit(vm: self)
            }
        }
    }
    
    weak var output: MoneyInputViewModelOutput?
}

extension MoneyInputViewModel {
    enum State {
        case display
        case edit(Edit)
        
        var isEdit: Bool {
            guard case .edit = self else {
                return false
            }
            
            return true
        }
    }
    
    mutating func startEdit() {
        state = .edit(.init())
    }
}

extension MoneyInputViewModel {
    struct Edit {
        func edit(vm: MoneyInputViewModel, inputText: String) throws -> MoneyInputViewModel {
            guard inputText.isEmpty == false else {
                return try edit(vm: vm, amount: 0)
            }
            /*if inputText.filter({ $0 == "." }).count == 1, inputText.split(separator: ".").count == 1 {
                throw Error.lastCharacterIsPoint
            }*/
            guard let amount = Float(inputText) else {
                throw Error.notNumber(text: inputText)
            }
            
            return try edit(vm: vm, amount: amount)
        }
        
        private func edit(vm: MoneyInputViewModel, amount: Float) throws -> MoneyInputViewModel {
            guard amount >= 0 else {
                throw Error.belowZero
            }
            
            guard amount < 999999 else {
                throw Error.tooBig
            }
            
            var newVM = vm
            newVM.amount = amount
            
            return newVM
        }
    }
}

extension MoneyInputViewModel.Edit {
    enum Error: Swift.Error {
        case notNumber(text: String)
        case belowZero
        case tooBig
        case lastCharacterIsPoint
        case unknown
    }
}


extension MoneyInputViewModel.State: Equatable {
    static func == (lhs: MoneyInputViewModel.State, rhs: MoneyInputViewModel.State) -> Bool {
        
        if case .display = lhs, case .display = rhs {
            return true
        } else if case .edit = lhs, case .edit = rhs {
            return true
        } else {
            return false
        }
    }
}

extension MoneyInputViewModel: Equatable {
    static func == (lhs: MoneyInputViewModel, rhs: MoneyInputViewModel) -> Bool {
        return
            lhs.currency == rhs.currency &&
            lhs.amount == rhs.amount &&
            lhs.state == rhs.state
    }
}
