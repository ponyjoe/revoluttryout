//
//  MoneyInputViewModelOutput.swift
//  RevoluttTestApp
//
//  Created by Константин on 15/02/2019.
//  Copyright © 2019 YK. All rights reserved.
//

import Foundation

protocol MoneyInputViewModelOutput: class {
    func didEdit(vm: MoneyInputViewModel)
    func didBeginEdit(vm: MoneyInputViewModel)
    func didEndEdit(vm: MoneyInputViewModel)
}

class MoneyInputViewModelOutputStub: MoneyInputViewModelOutput {
    func didEdit(vm: MoneyInputViewModel) {
        // none
    }
    
    func didBeginEdit(vm: MoneyInputViewModel) {
        // none
    }
    
    func didEndEdit(vm: MoneyInputViewModel) {
        // none
    }
}
